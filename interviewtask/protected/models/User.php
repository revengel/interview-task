<?php

/**
 * This is the model class for table "users".
 *
 * The followings are the available columns in table 'users':
 * @property integer $id
 * @property string $username
 * @property string $first_name
 * @property string $last_name
 * @property integer $education_id
 *
 * The followings are the available model relations:
 * @property Education $education
 * @property Cities[] $cities
 */
class User extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'users';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('username, education_id', 'required'),
			array('education_id', 'numerical', 'integerOnly'=>true),
			array('username, first_name, last_name', 'length', 'max'=>128),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, username, first_name, last_name, education_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'education' => array(self::BELONGS_TO, 'Education', 'education_id'),
			'cities' => array(self::MANY_MANY, 'City', 'users_cities(user_id, city_id)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'username' => 'Username',
			'first_name' => 'First Name',
			'last_name' => 'Last Name',
			'education_id' => 'Education',
			'education' => 'Education',
			'cities'    => 'Cities',
		);
	}


	public function behaviors(){
		return array('ESaveRelatedBehavior' => array(
			'class' => 'application.components.ESaveRelatedBehavior')
		);
	}


	public function has_education( $education ){
		$education = is_array($education) ? $education : array($education);
		$this->getDbCriteria()->mergeWith(array(
			'condition'=>'education_id IN (:ed_ids)',
			'params'=>array(':ed_ids'=>implode(',',$education)),
		));
		return $this;
	}

	public function has_city( $cities ){
		$cities = is_array($cities) ? $cities : array($cities);
		$this->getDbCriteria()->mergeWith(array(
			'condition'=>'cities.id IN (:cities_ids)',
			'params'=>array(':cities_ids'=>implode(',', $cities)),
		));
		return $this;
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('first_name',$this->first_name,true);
		$criteria->compare('last_name',$this->last_name,true);
		$criteria->compare('education_id',$this->education_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	public function getDataForGrid( $filters = array() ){
		$data = self::model()->with('education', 'cities')->findAll();
		var_dump($data);
	}


	public function getUsersDataProviderWithFilter(Array $filters = array()) {
		$model = self::model()->with(array(
			'cities'=>array('together'=>true),
			'education'=>array('together'=>true)
		));

		if (!empty($filters['education']))
			$model = $model->has_education($filters['education']);

		if (!empty($filters['cities']))
			$model = $model->has_city($filters['cities']);

//		$dataProvider = new CActiveDataProvider($model);
		return $model;
	}
}



