<?php

class UserController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view', 'ajax'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$model = $this->loadModel($id);
		$this->render('view',array(
			'model'=>$model,
			'citiesString' => $this->renderCitiesListByComma($model, 0),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new User;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];
			$model->cities=$_POST['User']['cities'];
			//if($model->save())
			if($model->saveWithRelated('cities'))
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
			'education_data' => Education::model()->findAll(),
			'cities_data'   => City::model()->findAll(),
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];
			$model->cities=$_POST['User']['cities'];
			if($model->saveWithRelated('cities'))
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
			'education_data' => Education::model()->findAll(),
			'cities_data'   => City::model()->findAll(),
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex__()
	{
		$dataProvider=new CActiveDataProvider('User');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new User('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['User']))
			$model->attributes=$_GET['User'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}


	public function actionIndex()
	{
		$filters = array();
		if(Yii::app()->request->isPostRequest){
			if ( $cities = Yii::app()->request->getPost('City') ) {
				$filters['cities'] = array_map('intval', $cities['city_name']);
			}

			if ($education = Yii::app()->request->getPost('Education') ) {
				$filters['education'] = array_map('intval', $education['education_name']);
			}
		}
		$dataProvider = new CActiveDataProvider( User::model()->getUsersDataProviderWithFilter($filters));


		$city_model = City::model();
		$education_model = Education::model();


		$this->render('filter',array(
			'dataProvider'=>$dataProvider,
			'city_model'         => $city_model,
			'education_model'   => $education_model,
			'city_model_data'   => $city_model->findAll(),
			'education_model_data'   => $education_model->findAll(),
		));
	}

	public function actionAjax() {
		$filters = array();
		if(Yii::app()->request->isPostRequest){
//			var_dump($_POST);
			if ( $cities = Yii::app()->request->getPost('cities') ) {
				$filters['cities'] = $cities;
			}

			if ($education = Yii::app()->request->getPost('education') ) {
				$filters['education'] = $education;
			}
		}


		$model = User::model()->getUsersDataProviderWithFilter($filters);
		$dataProvider = new CActiveDataProvider( $model );
		$dataProvider->pagination = false;
		$data = $dataProvider->getData();

		$out = array();

		foreach($data as $item) {
			$out[] = array(
				'id' => $item->id,
				'username' => $item->username,
				'first_name' => $item->first_name,
				'last_name' => $item->last_name,
				'education' => $item->education->education_name,
				'cities' => $this->renderCitiesListByComma($item,0),
			);
		}

		$this->renderJSON(array('data'=>$out));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return User the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=User::model()->with('education', 'cities')->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param User $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='user-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}




	public function renderCitiesListByComma($data, $row) {
		$out = array();
		$limit = 30;
		foreach ($data->cities as $city)
			$out[] = $city->city_name;
		$out = implode(', ', $out);
		return $out ? $out : '<No selected>';
	}


	/**
	 * Return data to browser as JSON and end application.
	 * @param array $data
	 */
	protected function renderJSON($data)
	{
		header('Content-type: application/json');
		echo CJSON::encode($data);

		foreach (Yii::app()->log->routes as $route) {
			if($route instanceof CWebLogRoute) {
				$route->enabled = false; // disable any weblogroutes
			}
		}
		Yii::app()->end();
	}


}
