<?php
/* @var $this MainController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'User',
);

?>

<h1>Users</h1>

<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'UsersFilterForm',
	'htmlOptions'=>array('class'=>'well'),
	'type' => 'horizontal',
)); ?>

<div class="row">
	<div class="span6">
		<?php echo $form->dropDownListRow($city_model, 'city_name', Chtml::listData($city_model_data,'id','city_name'), array('multiple'=>true, )); ?>
	</div>
	<div class="span6">
		<?php echo $form->dropDownListRow($education_model, 'education_name', Chtml::listData($education_model_data,'id','education_name'), array('multiple'=>true, )); ?>
	</div>
</div>


<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'label'=>'Filter', 'type'=>'primary', 'htmlOptions'   => array('id'=> 'UsersFilterButton'))); ?>

<?php $this->endWidget(); ?>

<div id="UsersExt4Grid"></div>


<?php
$this->widget('bootstrap.widgets.TbGridView', array(
	'dataProvider'=>$dataProvider,
	'columns'=>array(
		'id',
		'username',
		'first_name',
		'last_name',
		'education.education_name',

		array(
			'name'=>'cities',
			'value'=> array($this, 'renderCitiesListByComma'),
		),

		array(            // display a column with "view", "update" and "delete" buttons
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
));


?>


