<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	'Users'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List User', 'url'=>array('index')),
	array('label'=>'Create User', 'url'=>array('create')),
	array('label'=>'Update User', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete User', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage User', 'url'=>array('admin')),
);
?>

<h1>View User #<?php echo $model->id; ?></h1>

<?php


$this->widget(
	'bootstrap.widgets.TbDetailView',
	array(
		'type'=>'bordered condensed',
		'data' => $model,
		'attributes' => array(
			'id',
			'username',
			'first_name',
			'last_name',
			'education.education_name',
			array(
				'name'=>'cities',
				'value'=> $citiesString,
				//'value'=> array($this, 'renderCitiesListByComma'),
			),
		),
	)
);

//$this->widget('zii.widgets.CDetailView',
//	array(
//		'data'=>$model,
//		'attributes'=>array(
//			'id',
//			'username',
//			'first_name',
//			'last_name',
//			'education.education_name',
//			array(
//				'name'=>'cities',
//				'value'=> array($this, 'renderCitiesListByComma'),
//			),
//		),
//	)
//);

?>
