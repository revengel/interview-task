<?php
/* @var $this EducationController */
/* @var $data Education */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('education_name')); ?>:</b>
	<?php echo CHtml::encode($data->education_name); ?>
	<br />


</div>