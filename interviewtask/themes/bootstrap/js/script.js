Ext.onReady(function() {

      $('#UsersFilterButton').on('click', function(e){
          var $this = $(this),
              form = $this.closest('form'),
              cities = form.find('select#City_city_name').val(),
              education = form.find('select#Education_education_name').val(),
              params = {
                  cities: cities,
                  education: education
              };
          e.preventDefault();
          store.getProxy().extraParams = params;
          store.load();
      })




    Ext.define('UsersModel', {
        extend: 'Ext.data.Model',
        fields: ['id', 'username', 'first_name', 'last_name', 'education', 'cities']
    });

    var proxy_url = baseUrl + '/index.php?r=user/ajax';

    var store = Ext.create('Ext.data.ArrayStore', {
        model: 'UsersModel',
        pageSize: 50,
        proxy: {
            type: 'ajax',
            url: proxy_url,
            reader: {
                type: 'json',
                root: 'data'
            },
            actionMethods: {
                read: 'POST'
            }
        },
        autoLoad: true
    });

    Ext.create('Ext.grid.Panel', {
        store: store,
        columns: [
            {
                text     : 'Id',
                width: 60,
                dataIndex: 'id'
            },
            {
                text     : 'Username',
                flex:1,
                dataIndex: 'username'
            },
            {
                text     : 'First name',
                flex:1,
                dataIndex: 'first_name'
            },
            {
                text     : 'Last name',
                flex:1,
                dataIndex: 'last_name'
            },
            {
                text     : 'Education',
                flex:1,
                dataIndex: 'education'
            },
            {
                text     : 'Cities',
                flex:1,
                dataIndex: 'cities'
            }
        ],
        height: 350,
        width: '100%',
        title: 'Users',
        renderTo: 'UsersExt4Grid',

        bbar: Ext.create('Ext.PagingToolbar', {
            store: store,
            displayInfo: true,
            displayMsg: 'Displaying users {0} - {1} of {2}',
            emptyMsg: "No users to display"
        })

    });



});